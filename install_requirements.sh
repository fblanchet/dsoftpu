#!/usr/bin/env bash
if [ -z ${VIRTUAL_ENV+x} ]
    then
        pip3 install --user virtualenv
        export PATH=$PATH:$HOME/.local/bin
        virtualenv -p python3 env
        source env/bin/activate
fi
source version.sh
source /opt/intel/compilers_and_libraries_${INTEL_YYYY}.1.${TBB_VERSION#*.}/linux/tbb/bin/tbbvars.sh 'intel64'
pip install -r requirements.txt