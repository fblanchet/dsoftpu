Installation
============

Clone the git repository from gitlab:

.. code-block:: console

    $ git clone https://gitlab.com/fblanchet/dsoftpu.git

Install additional dependencies:

.. code-block:: console

    $ source build-dep.sh 2019

It adds Intel repositories to apt source list, asks you to select Intel IPP and TBB versions and installs the packages.

Setup and activate Python virtual environment and environment variables:

.. code-block:: console

    $ source install_requirements.sh
