## Introduction to DSoftPU

Digitiser Software Processing Unit

C++ & Python library intended to interface digitisers and allow use of Intel SIMD instruction sets to perform signal processing.