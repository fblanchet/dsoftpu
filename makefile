ARCH		= -march=native
DEBUG		= #-g
GENERATION  = -fPIC -fvisibility=hidden -fvisibility-inlines-hidden
INCLUDES	= -I$TBBROOT/include
OPTIMIZE	= -Ofast
VERBOSE		= #-v
WARNING		= -Wpedantic -Wall -Wextra -Wno-reorder

CXX	        = g++
CXXFLAGS	= $(ARCH) $(DEBUG) $(GENERATION) $(INCLUDES) $(OPTIMIZE) $(VERBOSE) $(WARNING) -std=c++17 -pthread

CC		    = g++ $(CXXFLAGS) -shared
LDFLAGS		=
LDLIBS      = -ltbb -lOpenCL
ifeq ($(OS),Windows_NT)
    LDLIBS +=
else
    LDLIBS += -ldl
endif

LIB		= lib
BIN		= bin
OBJ		= obj
SRC		= src
TARGET	= dummy

.PHONY:		all
all:		$(TARGET)

$(TARGET):	$(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(wildcard $(SRC)/*.cpp))
	@echo "Rule: " $@ ":" $^
	mkdir -p $(LIB)
	$(CC) $(LDFLAGS) $^ $(LDLIBS) -o $(LIB)/$@

$(OBJ)/%.o:	$(SRC)/%.cpp
	@echo "Rule: " $@ ":" $^
	mkdir -p $(OBJ)
	$(CXX) $(CXXFLAGS) -c -o $(OBJ)/$*.o $<

.PHONY:		clean
clean:
	rm $(VERBOSE) -f $(wildcard $(OBJ)/*.o)
