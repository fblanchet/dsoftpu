import unittest


class TestDummy(unittest.TestCase):
    def test_dummy(self):
        self.assertTrue(True)


if __name__ == '__main__':
    with open('unittest.log', "w+") as log_file:
        unittest.main(testRunner=unittest.TextTestRunner(log_file, verbosity=2, buffer=True))
