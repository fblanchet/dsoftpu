#!/usr/bin/env bash
apt-get update -y
apt-get install -y apt-transport-https

mkdir -p tmp
wget https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS-$1.PUB -O tmp/GPG-pub-key-Intel.pub
apt-key add tmp/GPG-pub-key-Intel.pub
echo deb https://apt.repos.intel.com/ipp all main > /etc/apt/sources.list.d/intel-ipp.list
echo deb https://apt.repos.intel.com/tbb all main > /etc/apt/sources.list.d/intel-tbb.list
apt-get update -y

mkdir -p log
apt-cache search intel-ipp-mt-devel-$1. > log/ipp_version.log
apt-cache search intel-tbb-devel-$1. > log/tbb_version.log
if [ $# -eq 1 ]
    then
        echo "$(cat log/ipp_version.log)"
        echo "Intel IPP version ?"
        read IPP

        echo "$(cat log/tbb_version.log)"
        echo "Intel TBB version ?"
        read TBB
elif [ $# -eq 3 ]
    then
        IPP=$2
        TBB=$3
else
    exit 1
fi
echo "#!/usr/bin/env bash" > version.sh
echo "export INTEL_YYYY=$1" >> version.sh
echo "Intel IPP version $1 ${IPP}"
echo "Intel IPP version $1 ${IPP}" >> log/ipp_version.log
echo "export IPP_VERSION=${IPP}" | sed "s/-/./g" >> version.sh
echo "Intel TBB version $1 ${TBB}"
echo "Intel TBB version $1 ${TBB}" >> log/tbb_version.log
echo "export TBB_VERSION=${TBB}" | sed "s/-/./g" >> version.sh

apt-get install -y build-essential gcc python3-pip
apt-get install -y libboost-dev intel-ipp-mt-devel-$1.${IPP} intel-tbb-devel-$1.${TBB}
apt-get install -y pciutils ocl-icd-opencl-dev clinfo