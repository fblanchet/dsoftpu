..  DSoftPU documentation master file, created by
    sphinx-quickstart on Sat Jan  5 23:50:20 2019.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to DSoftPU's documentation!
===================================

.. mdinclude:: ../../README.md

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/installation
   license

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
