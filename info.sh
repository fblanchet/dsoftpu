#!/usr/bin/env bash
mkdir -p log
cat /proc/cpuinfo > log/cpu.log
gcc -march=native -Q --help=target > log/gcc.log
lspci > log/pci.log
clinfo > log/opencl.log